package br.com.mauryoshiro.geradorjogosloteria.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class TipoJogo {
	
	private String nome;
	private Integer numeroMinimo;
	private Integer	numeroMaximo;

}
