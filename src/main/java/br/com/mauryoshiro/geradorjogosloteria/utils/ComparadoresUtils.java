package br.com.mauryoshiro.geradorjogosloteria.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ComparadoresUtils {

	public static boolean comparadorMapValoresStringRepetidos(Map<Integer, String> map) {
		Map<Integer, Integer> keysVerificadas = new HashMap<>(map.size()*2);
		Set<Entry<Integer, String>> entrySet = map.entrySet();
		for(Entry<Integer, String> entry : entrySet) {
			Integer contadorValueEncontrado = 0;
			Integer key = entry.getKey();
			String value = entry.getValue();
			
			for(Entry<Integer, String> entry2 : entrySet) {
				Integer key2 = entry2.getKey();
				if(!keysVerificadas.containsKey(key2)) {
					String value2 = entry2.getValue();
					if(value2.hashCode() == value.hashCode()) {
						contadorValueEncontrado++;
					}
				}
			}
			
			keysVerificadas.put(key, 0);
			
			if(contadorValueEncontrado > 1) {
				return true;
			}
		}
		
		return false;
	}
}
