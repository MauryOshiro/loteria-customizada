package br.com.mauryoshiro.geradorjogosloteria;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

import com.google.common.math.BigIntegerMath;

import br.com.mauryoshiro.geradorjogosloteria.model.TipoJogo;

public class GeradorJogosLoteria {

	public static void main(String[] args) {
		System.out.println("==========CONFIGURAÇÃO=========");
		System.out.println("INICIANDO GERAÇÃO DE JOGOS");
		
		Map<Integer, TipoJogo> tiposJogos = preparaMapTiposJogos();
		Scanner scanner = new Scanner(System.in);
		TipoJogo tipoJogoSelecionado = selecaoJogo(tiposJogos, scanner);
		System.out.println("Jogo selecionado: " + tipoJogoSelecionado.getNome());
		
		Integer qtdJogos = 0;
		Integer qtdNumeros = 0;
		boolean permitidoGeracaoJogos = true;
		
		do {
			qtdJogos = preparaQuantidadeJogos(scanner);
			qtdNumeros = preparaQuantidadeNumeros(scanner);
			permitidoGeracaoJogos = quantidadeJogosValido(tipoJogoSelecionado, qtdNumeros, qtdJogos);
			
			if(permitidoGeracaoJogos) {
				System.out.println("Quantidade de jogos e quantidade de números por jogo são válidos.");
			} else {
				System.err.println("Impossível gerar a quantidade de jogos com a quantidade de números por jogo solicitada!");
				System.err.println("Por favor, digite novamente a quantidade de jogos e números por jogo.");
			}
			
		} while (!permitidoGeracaoJogos);

		System.out.println("Quantidade de jogos a serem gerados " + qtdJogos);
		System.out.println("Os jogos terão " + qtdNumeros + " números");
		System.out.println("==========CONFIGURAÇÃO=========");

		System.out.println("==========GERAÇÃO=========");
		HashMap<Integer, String> jogos = new HashMap<>(qtdJogos * 2);

		LocalDateTime inicio = LocalDateTime.now();
		jogos = gerarJogos(jogos, qtdNumeros, tipoJogoSelecionado, qtdJogos);
		LocalDateTime fimGeracao = LocalDateTime.now();
		Long tempoGeracao = inicio.until(fimGeracao, ChronoUnit.MILLIS);
			
		System.out.println("==========GERAÇÃO=========");

		System.out.println("==========RESULTADO==========");
		exibeJogos(jogos);
		System.out.println("---------------------------------");
		System.out.println("Quantidade de jogos gerados: " + qtdJogos);
		System.out.println("Cada jogo possui " + qtdNumeros + " números");	
		
		LocalDateTime fimProcessamento = LocalDateTime.now();
		Long tempoTotal = inicio.until(fimProcessamento, ChronoUnit.MILLIS); 

		if (qtdJogos.equals(jogos.values().size())) {
			System.out.println("Jogos gerados com sucesso!");
		} else {
			System.err.println("ERRO: NÃO FOI GERADO A QUANTIDADE ESPERADA DE JOGOS!!!");
		}
		System.out.println("Tempo de geração dos jogos: " + tempoGeracao + " milissegundos");
		System.out.println("Tempo total (contando tempo para exibir no console): " + tempoTotal + " milissegundos");

		System.out.println("==========RESULTADO==========");
		scanner.close();
		System.out.println("FIM");
	}
	
	private static Map<Integer, TipoJogo> preparaMapTiposJogos() {
		Map<Integer, TipoJogo> tiposJogos = new HashMap<>();
		
		TipoJogo megaSena = new TipoJogo();
		megaSena.setNome("Mega-Sena");
		megaSena.setNumeroMaximo(60);
		megaSena.setNumeroMinimo(1);
		
		TipoJogo quina = new TipoJogo();
		quina.setNome("Quina");
		quina.setNumeroMinimo(1);
		quina.setNumeroMaximo(50);
		
		tiposJogos.put(1, megaSena);
		tiposJogos.put(2, quina);
		
		return tiposJogos;
	}
	
	private static TipoJogo selecaoJogo(Map<Integer, TipoJogo> tiposJogos, Scanner scanner) {
		boolean tipoInvalido = true;
		Integer jogoSelecionado = 0;
		
		while(tipoInvalido) {
			Set<Entry<Integer, TipoJogo>> entrySet = tiposJogos.entrySet();
			for(Entry<Integer, TipoJogo> jogo : entrySet) {
				System.out.println(jogo.getKey() + " - " + jogo.getValue().getNome());
			}
			System.out.println("--------------------------------------------");
			
			System.out.print("Digite o código do jogo desejado: ");
			String jogoDigitado = scanner.nextLine();
			
			if(NumberUtils.isParsable(jogoDigitado)) {
				try {
					jogoSelecionado = Integer.parseInt(jogoDigitado);
					if(tiposJogos.containsKey(jogoSelecionado)) {
						tipoInvalido = false;
					} else {
						System.out.println("Esse código de jogo não existe.");
					}
				} catch (Exception e) {
					System.out.println("Código inválido.");
				}
			} else {
				System.out.println("Código inválido.");
			}

			System.out.println("--------------------------------------------");
		}
		
		return tiposJogos.get(jogoSelecionado);
	}
	
	private static Integer preparaQuantidadeJogos(Scanner scanner) {
		boolean qtdInvalida = true;
		Integer qtdDesejada = 0;
		
		while(qtdInvalida) {
			System.out.print("Digite a quantidade de jogos que serão feitos: ");
			String quantidadeDigitada = scanner.nextLine();
			
			if(NumberUtils.isParsable(quantidadeDigitada)) {
				try {
					qtdDesejada = Integer.parseInt(quantidadeDigitada);
					qtdInvalida = false;
				} catch (Exception e) {
					System.out.println("Quantidade inválida.");
				}
			} else {
				System.out.println("Quantidade inválida.");
			}
		}
		
		return qtdDesejada;
	}
	
	public static boolean quantidadeJogosValido(TipoJogo tipoJogo, Integer qtdNumerosDesejada, Integer qtdJogos) {
		Integer qtdNumeros = tipoJogo.getNumeroMaximo();
		BigInteger qtdJogosPossiveis = BigIntegerMath.binomial(qtdNumeros, qtdNumerosDesejada);
		if(qtdJogosPossiveis.compareTo(BigInteger.valueOf(qtdJogos.longValue())) >= 0) {
			return true;
		}
		
		return false;
	}
	
	private static Integer preparaQuantidadeNumeros(Scanner scanner) {
		boolean quantidadeInvalida = true;
		Integer quantidadeDesejada = 0;
		
		while(quantidadeInvalida) {
			System.out.print("Digite a quantidade de números que serão sorteados: ");
			String quantidadeDigitada = scanner.nextLine();
			
			if(NumberUtils.isParsable(quantidadeDigitada)) {
				try {
					quantidadeDesejada = Integer.parseInt(quantidadeDigitada);
					quantidadeInvalida = false;
				} catch (Exception e) {
					System.out.println("Quantidade inválida.");
				}
			} else {
				System.out.println("Quantidade inválida.");
			}
		}
		
		return quantidadeDesejada;
	}

	public static HashMap<Integer, String> gerarJogos(HashMap<Integer, String> jogos, Integer qtdNumeros,
			TipoJogo tipoJogo, Integer qtdJogos) {
		
		for(Integer i = 0; i < qtdJogos; i++) {
			List<Integer> listaNumeros = new ArrayList<>(qtdNumeros + 5);
			boolean listaNumerosNaoPronta = true;
			
			while (listaNumerosNaoPronta) {
				Integer numero = ThreadLocalRandom.current().nextInt(tipoJogo.getNumeroMinimo(), 
						tipoJogo.getNumeroMaximo() + 1);
				
				if (!listaNumeros.contains(numero)) {
					listaNumeros.add(numero);
					
					if (listaNumeros.size() == qtdNumeros) {
						Collections.sort(listaNumeros);
						
						List<String> listaString = listaNumeros.stream().map(item -> {
							String numeroString = StringUtils.leftPad(item.toString(), 2, "0");
							return numeroString;
						}).collect(Collectors.toList());
						
						String jogo = StringUtils.EMPTY;
						
						for(String numeroString : listaString) {
							jogo = jogo.concat(numeroString).concat(" | ");
						}
						
						boolean jogoValido = verificaValidadeJogo(jogos, jogo);
						if (jogoValido) {
							jogos.put(jogo.hashCode(), jogo);
//							System.out.println("Jogo gerado: " + numeroJogo);
//							System.out.println("Números desse jogo: " + listaIntegerToString(listaNumeros));
							listaNumerosNaoPronta = false;
						} else {
							System.out.println("ZERANDO JOGO DE NÚMERO " + i);
							listaNumeros.clear();
						}
					}
				}
			}			
		}

		return jogos;
	}

	private static boolean verificaValidadeJogo(Map<Integer, String> jogos, String jogo) {
		if (jogos.isEmpty()) {
			return true;
		}

		if(jogos.containsKey(jogo.hashCode())) {
			System.out.println("AVISO: FOI GERADO UM JOGO REPETIDO. SERÁ GERADO UM NOVO JOGO");
			System.out.println("JOGO JÁ FEITO: " + jogo);
			System.out.println("JOGO GERADO AGORA: " + jogo);
			return false;
		}
		
		return true;
	}
	
	private static void exibeJogos(Map<Integer, String> jogos) {
		Integer numeroJogo = 1;
		Set<Entry<Integer, String>> entrySet = jogos.entrySet();
		for(Entry<Integer, String> entry : entrySet) {
			System.out.println("Jogo " + numeroJogo + ": " + entry.getValue());
			numeroJogo++;
		}	
	}

}
