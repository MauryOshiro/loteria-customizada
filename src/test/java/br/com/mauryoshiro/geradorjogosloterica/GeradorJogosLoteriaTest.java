package br.com.mauryoshiro.geradorjogosloterica;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

import br.com.mauryoshiro.geradorjogosloteria.GeradorJogosLoteria;
import br.com.mauryoshiro.geradorjogosloteria.model.TipoJogo;
import br.com.mauryoshiro.geradorjogosloteria.utils.ComparadoresUtils;

class GeradorJogosLoteriaTest {

	@Test
	void testGeracaoJogosCom6Numeros() {
		System.out.println("==================================");
		System.out.println(this.getClass().getSimpleName());
		System.out.println("testGeracaoJogos - Início");
		Integer qtdJogos = 100000;
		Integer qtdNumeros = 6;
		TipoJogo tipoJogo = new TipoJogo();
		tipoJogo.setNome("Mega-Sena");
		tipoJogo.setNumeroMinimo(1);
		tipoJogo.setNumeroMaximo(60);
		
		LocalDateTime inicioGeracao = LocalDateTime.now(); 
		HashMap<Integer, String> jogos = new HashMap<>();
		
		jogos = GeradorJogosLoteria.gerarJogos(jogos, qtdNumeros, tipoJogo, qtdJogos);
		
		LocalDateTime fimGeracao = LocalDateTime.now();
		
		Long tempoGeracao = ChronoUnit.SECONDS.between(inicioGeracao, fimGeracao);
		System.out.println("Foi necessário " + tempoGeracao + " segundos para gerar " + qtdJogos + " jogos");
		
		System.out.println("Jogos gerados. Iniciando verificação de jogos repetidos");
		
		LocalDateTime inicioValidacao = LocalDateTime.now();
		boolean existeJogosRepetidos = ComparadoresUtils.comparadorMapValoresStringRepetidos(jogos);
		LocalDateTime fimValidacao = LocalDateTime.now();
		Long tempoValidacao = ChronoUnit.SECONDS.between(inicioValidacao, fimValidacao);
		System.out.println("Foi necessário " + tempoValidacao + " segundos para validar todos os jogos");	
		
		assertEquals(qtdJogos, jogos.values().size(), "número de jogos gerados deve ser igual "
				+ "a quantidade solicitada");
		assertFalse(existeJogosRepetidos, "Não pode haver jogos repetidos");
		System.out.println("testGeracaoJogos - Fim");
		System.out.println("==================================");
	}
	
	@Test
	void testGeracaoJogosCom7Numeros() {
		System.out.println("==================================");
		System.out.println(this.getClass().getSimpleName());
		System.out.println("testGeracaoJogos - Início");
		Integer qtdJogos = 100000;
		Integer qtdNumeros = 7;
		TipoJogo tipoJogo = new TipoJogo();
		tipoJogo.setNome("Mega-Sena");
		tipoJogo.setNumeroMinimo(1);
		tipoJogo.setNumeroMaximo(60);
		
		LocalDateTime inicioGeracao = LocalDateTime.now(); 
		HashMap<Integer, String> jogos = new HashMap<>();
		
		jogos = GeradorJogosLoteria.gerarJogos(jogos, qtdNumeros, tipoJogo, qtdJogos);
		
		LocalDateTime fimGeracao = LocalDateTime.now();
		
		Long tempoGeracao = ChronoUnit.SECONDS.between(inicioGeracao, fimGeracao);
		System.out.println("Foi necessário " + tempoGeracao + " segundos para gerar " + qtdJogos + " jogos");
		
		System.out.println("Jogos gerados. Iniciando verificação de jogos repetidos");
		
		LocalDateTime inicioValidacao = LocalDateTime.now();
		boolean existeJogosRepetidos = ComparadoresUtils.comparadorMapValoresStringRepetidos(jogos);
		LocalDateTime fimValidacao = LocalDateTime.now();
		Long tempoValidacao = ChronoUnit.SECONDS.between(inicioValidacao, fimValidacao);
		System.out.println("Foi necessário " + tempoValidacao + " segundos para validar todos os jogos");	
		
		assertEquals(qtdJogos, jogos.values().size(), "número de jogos gerados deve ser igual "
				+ "a quantidade solicitada");
		assertFalse(existeJogosRepetidos, "Não pode haver jogos repetidos");
		System.out.println("testGeracaoJogos - Fim");
		System.out.println("==================================");
	}
	
	@Test
	void testGeracaoJogosCom9Numeros() {
		System.out.println("==================================");
		System.out.println(this.getClass().getSimpleName());
		System.out.println("testGeracaoJogos - Início");
		Integer qtdJogos = 100000;
		Integer qtdNumeros = 9;
		TipoJogo tipoJogo = new TipoJogo();
		tipoJogo.setNome("Mega-Sena");
		tipoJogo.setNumeroMinimo(1);
		tipoJogo.setNumeroMaximo(60);
		
		LocalDateTime inicioGeracao = LocalDateTime.now(); 
		HashMap<Integer, String> jogos = new HashMap<>();
		
		jogos = GeradorJogosLoteria.gerarJogos(jogos, qtdNumeros, tipoJogo, qtdJogos);
		
		LocalDateTime fimGeracao = LocalDateTime.now();
		
		Long tempoGeracao = ChronoUnit.SECONDS.between(inicioGeracao, fimGeracao);
		System.out.println("Foi necessário " + tempoGeracao + " segundos para gerar " + qtdJogos + " jogos");
		
		System.out.println("Jogos gerados. Iniciando verificação de jogos repetidos");
		
		LocalDateTime inicioValidacao = LocalDateTime.now();
		boolean existeJogosRepetidos = ComparadoresUtils.comparadorMapValoresStringRepetidos(jogos);
		LocalDateTime fimValidacao = LocalDateTime.now();
		Long tempoValidacao = ChronoUnit.SECONDS.between(inicioValidacao, fimValidacao);
		System.out.println("Foi necessário " + tempoValidacao + " segundos para validar todos os jogos");	
		
		assertEquals(qtdJogos, jogos.values().size(), "número de jogos gerados deve ser igual "
				+ "a quantidade solicitada");
		assertFalse(existeJogosRepetidos, "Não pode haver jogos repetidos");
		System.out.println("testGeracaoJogos - Fim");
		System.out.println("==================================");
	}
	
	@Test
	void testGeracaoJogosCom13Numeros() {
		System.out.println("==================================");
		System.out.println(this.getClass().getSimpleName());
		System.out.println("testGeracaoJogos - Início");
		Integer qtdJogos = 100000;
		Integer qtdNumeros = 13;
		TipoJogo tipoJogo = new TipoJogo();
		tipoJogo.setNome("Mega-Sena");
		tipoJogo.setNumeroMinimo(1);
		tipoJogo.setNumeroMaximo(60);
		
		LocalDateTime inicioGeracao = LocalDateTime.now(); 
		HashMap<Integer, String> jogos = new HashMap<>();
		
		jogos = GeradorJogosLoteria.gerarJogos(jogos, qtdNumeros, tipoJogo, qtdJogos);
		
		LocalDateTime fimGeracao = LocalDateTime.now();
		
		Long tempoGeracao = ChronoUnit.SECONDS.between(inicioGeracao, fimGeracao);
		System.out.println("Foi necessário " + tempoGeracao + " segundos para gerar " + qtdJogos + " jogos");
		
		System.out.println("Jogos gerados. Iniciando verificação de jogos repetidos");
		
		LocalDateTime inicioValidacao = LocalDateTime.now();
		boolean existeJogosRepetidos = ComparadoresUtils.comparadorMapValoresStringRepetidos(jogos);
		LocalDateTime fimValidacao = LocalDateTime.now();
		Long tempoValidacao = ChronoUnit.SECONDS.between(inicioValidacao, fimValidacao);
		System.out.println("Foi necessário " + tempoValidacao + " segundos para validar todos os jogos");	
		
		assertEquals(qtdJogos, jogos.values().size(), "número de jogos gerados deve ser igual "
				+ "a quantidade solicitada");
		assertFalse(existeJogosRepetidos, "Não pode haver jogos repetidos");
		System.out.println("testGeracaoJogos - Fim");
		System.out.println("==================================");
	}
	
	@Test
	void testGeracaoJogosCom15Numeros() {
		System.out.println("==================================");
		System.out.println(this.getClass().getSimpleName());
		System.out.println("testGeracaoJogos - Início");
		Integer qtdJogos = 100000;
		Integer qtdNumeros = 15;
		TipoJogo tipoJogo = new TipoJogo();
		tipoJogo.setNome("Mega-Sena");
		tipoJogo.setNumeroMinimo(1);
		tipoJogo.setNumeroMaximo(60);
		
		LocalDateTime inicioGeracao = LocalDateTime.now(); 
		HashMap<Integer, String> jogos = new HashMap<>();
		
		jogos = GeradorJogosLoteria.gerarJogos(jogos, qtdNumeros, tipoJogo, qtdJogos);
		
		LocalDateTime fimGeracao = LocalDateTime.now();
		
		Long tempoGeracao = ChronoUnit.SECONDS.between(inicioGeracao, fimGeracao);
		System.out.println("Foi necessário " + tempoGeracao + " segundos para gerar " + qtdJogos + " jogos");
		
		System.out.println("Jogos gerados. Iniciando verificação de jogos repetidos");
		
		LocalDateTime inicioValidacao = LocalDateTime.now();
		boolean existeJogosRepetidos = ComparadoresUtils.comparadorMapValoresStringRepetidos(jogos);
		LocalDateTime fimValidacao = LocalDateTime.now();
		Long tempoValidacao = ChronoUnit.SECONDS.between(inicioValidacao, fimValidacao);
		System.out.println("Foi necessário " + tempoValidacao + " segundos para validar todos os jogos");	
		
		assertEquals(qtdJogos, jogos.values().size(), "número de jogos gerados deve ser igual "
				+ "a quantidade solicitada");
		assertFalse(existeJogosRepetidos, "Não pode haver jogos repetidos");
		System.out.println("testGeracaoJogos - Fim");
		System.out.println("==================================");
	}

}
